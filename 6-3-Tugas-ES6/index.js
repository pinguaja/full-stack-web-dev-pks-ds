//Soal 1
//buatlah fungsi menggunakan arrow function luas dan keliling persegi 
//panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
let hitungLuasPersegiPanjang = (panjang, lebar)=>{
    return panjang * lebar
}
let hitungKelilingPersegiPanjang = (panjang, lebar)=>{
    return 2 * (panjang + lebar);
}

//soal 2
const newFunction = (firstName, lastName)=>{
    return {
      firstName,
      lastName,
      fullName: ()=>{
        console.log(`${firstName} ${lastName}`)
      }
    }  
}
//Driver Code 
newFunction("William", "Imoh").fullName()

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

//soal 4
//Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west,...east]
//Driver Code
console.log(combined)

//soal 5
//sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)
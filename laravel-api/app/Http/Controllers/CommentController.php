<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
     //proteksi terhadap user yang belum login
     public function __construct()
     {
      return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coment = Comments::all();

        return response()->json([
            'success' => true,
            'message' => 'List Komentar',
            'data'    => $coment  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = auth()->user();
        //save to database
        $komen = Comments::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id,
            'user_id' => $user->id
        ]);
       
        /*
        //ini kirim ke pemilik post
        Mail::to($komen->post->user->email)->send(new PostAuthorMail($komen));
        //ini kirim ke pemilik komen
        Mail::to($komen->user->email)->send(new CommentAuthorMail($komen));
        */
        //memanggil event commentstoredevent
        event(new CommentStoredEvent($komen));
        //success save to database
        if($komen) {

            return response()->json([
                'success' => true,
                'message' => 'Komentar Berhasil ditambahkan',
                'data'    => $komen 
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Komentar gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //cari komentar berdasarkan id
        $komen = Comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Komentar',
            'data'    => $komen 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $komen = Comments::findOrFail($id);

        if($komen) {
            $user = auth()->user();
            //cek kepunyaan komen milik user
            if ($komen->user->id != $user->id) 
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Komen gagal diubah karna bukan milik anda',
                    'data'    => $komen  
                ], 200);
            }
            //update comment
            $komen->update([
                'content'     => $request->content,
                'post_id'   => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Komentar Berhasil diUbah',
                'data'    => $komen 
            ], 200);
        }
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $komen= Comments::findOrfail($id);

        if($komen) {
            $user = auth()->user();
            //cek kepunyaan komen milik user
            if ($komen->user->id != $user->id) 
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Komen gagal dihapus karna bukan milik anda',
                    'data'    => $komen  
                ], 200);
            }
            //delete post
            $komen->delete();

            return response()->json([
                'success' => true,
                'message' => 'Komentar terhapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Komentar tidak ditemukan',
        ], 404);
    
    }
}
